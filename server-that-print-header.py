from flask import Flask
from flask import request
app = Flask(__name__)


@app.route('/')
def hello_world():
    output = []
    for header in request.headers:
        output.append('{} ==> {}'.format(*header))
    fmt = 'request headers are: {}'
    return fmt.format(' '.join(output))
