import requests

headers = {'hfeng-add-header': 'hello-world'}
r = requests.get('http://localhost:5000', headers=headers)

print(r.status_code)
print(r.text)

# <===================OUTPUT===================>
# 200
# request headers are: User-Agent ==> python-requests/2.18.4 Connection ==> keep-alive Hfeng-Add-Header ==> hello-world Accept ==> */* Host ==> localhost:5000 Accept-Encoding ==> gzip, deflate
