# Read Me

## Prepare
```
sudo pip install -r requirements.txt
```

## Start server
```
bash run.sh
```

## Run the client
```
$ python http-request-example.py
200
request headers are: User-Agent ==> python-requests/2.18.4 Connection ==> keep-alive Hfeng-Add-Header ==> hello-world Accept ==> */* Host ==> localhost:5000 Accept-Encoding ==> gzip, deflate
```
